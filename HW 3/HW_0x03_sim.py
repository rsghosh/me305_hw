'''@file                    HW_0x03_sim.py
   @brief                   This file contains the simulation implementation for HW 0x03.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 1, 2021
'''

import numpy as np
import matplotlib.pyplot as plt

def sim(A, B, x0, u0, K, duration, t_inc):
    '''@brief           Generates simulation results.
       @details         Returns lists for time, state variables, and inputs over a time period.
       @param A         A matrix.
       @param B         B matrix.
       @param x0        Initial state vector.
       @param u0        Initial input vector.
       @param K         Gains.
       @param duration  Length of time in seconds to run the simulation.
       @param t_inc     Length of time in seconds to increment between each loop.
       @return          Time array, state variable matrix, and input matrix.
    '''
    
    # get number of samples
    sample_count = int(duration/t_inc + 1)

    # set up arrays to store data
    t_data = np.zeros(sample_count)
    x_data = np.zeros((4, sample_count))
    u_data = np.zeros(sample_count)

    # run sim
    x = x0
    u = u0
    for i in range(sample_count):
        # store data
        t_data[i] = i*t_inc
        x_data[:, [i]] = x
        u_data[i] = u

        # get torque input
        u = get_input(K, x)

        # calculate next x
        x = np.add(x, (np.matmul(A, x) + np.matmul(B, u)) * t_inc)

    return t_data, x_data, u_data

def get_input(K, x):
    '''@brief           Returns an input torque Tx
       @details         Returns an input torque Tx for a given K vector.
       @param K         Gains.
       @param x         State vector.
       @return          Tx (Motor torque input).
    '''
    return np.atleast_2d(-np.matmul(K, x))

def make_plots(t_data, x_data, title, file_prefix):
    '''@brief               Create plots from simulation data.
       @details             Creates a plot of each state vector versus time and saves it as an image.
       @param t_data        Time array.
       @param x_data        State vector matrix.
       @param title         Base title for each plot.
       @param file_prefix   Prefix for the image files.
    '''

    # x vs t
    plt.title(title + ': x vs t')
    plt.xlabel('t (s)')
    plt.ylabel('x (m)')
    plt.plot(t_data, x_data[0,:])
    plt.savefig(file_prefix + 'x.png')
    plt.show()

    # theta_y vs t
    plt.title(title + ': theta_y vs t')
    plt.xlabel('t (s)')
    plt.ylabel('theta_y (rad)')
    plt.plot(t_data, x_data[1,:])
    plt.savefig(file_prefix + 'theta_y.png')
    plt.show()

    # x_dot vs t
    plt.title(title + ': x_dot vs t')
    plt.xlabel('t (s)')
    plt.ylabel('x_dot (m/s)')
    plt.plot(t_data, x_data[2,:])
    plt.savefig(file_prefix + 'x_dot.png')
    plt.show()

    # theta_dot_y vs t
    plt.title(title + ': theta_dot_y vs t')
    plt.xlabel('t (s)')
    plt.ylabel('theta_dot_y (rad/s)')
    plt.plot(t_data, x_data[3,:])
    plt.savefig(file_prefix + 'theta_dot_y.png')
    plt.show()

if __name__ == '__main__':
    # A and B matrices
    ## A matrix
    A = np.array([[0, 0, 1, 0],
                  [0, 0, 0, 1],
                  [-5.2170, 4.0111, 0, 0.1773],
                  [112.8871, 64.8295, 0, -3.8358]])
    
    ## B matrix
    B = np.array([[0],
                  [0],
                  [32.4991],
                  [-703.2272]])

    # initial conditions
    ## Initial state vector
    x0 = np.transpose(np.atleast_2d(np.array([0, 0, 0, 0])))

    ## Initial input vector
    u0 = np.atleast_2d(np.array([0]))
    
    ## Gains
    K = np.array([0, 0, 0, 0])

    # time settings
    ## Duration of the simulation in seconds
    duration = 1

    ## Time in seconds to increment between each loop
    t_inc = 0.01

    # 1st sim run - at rest, x0=0m, Tx0=0, open-loop
    t_data, x_data, u_data = sim(A, B, x0, u0, K, duration, t_inc)
    make_plots(t_data, x_data, 'Open-loop, x0=0m', 'HW0x03_sim1_')

    # 2nd sim run - at rest, x0=0.05m, Tx0=0, open-loop
    x0 = np.transpose(np.atleast_2d(np.array([0.05, 0, 0, 0])))
    duration = 0.4
    t_data, x_data, u_data = sim(A, B, x0, u0, K, duration, t_inc)
    make_plots(t_data, x_data, 'Open-loop, x0=0.05m', 'HW0x03_sim2_')

    # 3rd sim run - at rest, x0=0.05m, Tx0=0, closed-loop
    K = np.array([-0.3, -0.2, -0.05, -0.02])
    duration = 30
    t_data, x_data, u_data = sim(A, B, x0, u0, K, duration, t_inc)
    make_plots(t_data, x_data, 'Closed-loop, x0=0.05m', 'HW0x03_sim3_')
